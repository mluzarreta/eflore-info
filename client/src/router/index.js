import Vue from 'vue'
import Router from 'vue-router'
import SearchInput from '@/components/SearchInput'
import SearchSpeciesLog from '@/components/SearchSpeciesLog'
import SpeciesCardsList from '@/components/SpeciesCardsList'
import SpeciesDetails from '@/components/SpeciesDetails'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/search'
    },
    {
      path: '/search',
      name: 'root',
      components: {
        SearchInput
      }
    },
    {
      path: '/search/:searchText',
      name: 'search',
      components: {
        SearchInput,
        SearchSpeciesLog,
        SpeciesCardsList,
     },
    },
    {
      path: '/detail/:speciesId',
      name: 'detail',
      components: {
        SpeciesDetails,
      },
    },
  ],
});

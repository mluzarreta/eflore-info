## E FLORE
[![Node version](https://img.shields.io/badge/npm-%3E%3D%203.0.0-green.svg)](http://nodejs.org/download/)
![Node version](https://img.shields.io/badge/node-%3E%3D%206.0.0-blue.svg)
![](https://img.shields.io/badge/vue-%5E2.5.2-green.svg)
[![Mac/Linux Build Status](https://img.shields.io/travis/typicode/hotel/master.svg?label=Mac%20OSX%20%26%20Linux)](https://travis-ci.org/typicode/hotel) [![Windows Build status](https://img.shields.io/appveyor/ci/typicode/hotel/master.svg?label=Windows)](https://ci.appveyor.com/project/typicode/hotel/branch/master)

[![devDependencies Status](https://david-dm.org/dwyl/hapi-auth-jwt2/dev-status.svg)](https://david-dm.org/dwyl/hapi-auth-jwt2?type=dev)

![img](./eflore.gif)
EFLORE works great on any OS (OS X, Linux, Windows) :heart:


## Prerequisites

I. **npm** is the recommended installation method for frontend part

   If this is the first time you want to give **npm** a shot, don't worry, we got exactly what you need.

   - [How to Install Node.js and NPM on Windows](http://blog.teamtreehouse.com/install-node-js-npm-windows)

   - [How to Install Node.js and NPM on a Mac](http://blog.teamtreehouse.com/install-node-js-npm-mac)

   - [How to Install Node.js and NPM on a Linux](http://blog.teamtreehouse.com/install-node-js-npm-linux

II. **python3** is needed too for backend part
    - type "install python**3** on <your OS>" in your favorite search engine

### client

    > A Vue.js project

### server
    
    > A Django project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).




# Installation

## Python3 Virtual Env creation

```
virtualenv -p python3 venv
source venv/bin/activate
```

## Update raw databases

```
inv updatedata
```

## Init and populate databases
```
inv initdb
```


## Invoke install

```
pip install -r server/requirements.txt
```

## Building project

```
inv build
```

## Running

```
inv rundev
```



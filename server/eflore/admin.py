from django.contrib import admin
from eflore.models import *

admin.site.register(Taxon)
admin.site.register(Species)
from django.urls import include, path
from rest_framework import routers

from eflore.views import SpeciesViewSet, TaxonViewSet

router = routers.DefaultRouter()
router.register('species', SpeciesViewSet)
router.register('taxon', TaxonViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
